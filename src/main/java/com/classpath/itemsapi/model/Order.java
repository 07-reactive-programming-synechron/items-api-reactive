package com.classpath.itemsapi.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Past;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table("orders")
public class Order {
    @Id
    private long orderId;

    @NotEmpty(message = "email address cannot be empty")
    @Email(message = "Email address is not correct")
    private String email;

    @Past(message = "Order date cannot be in future")
    private LocalDate orderDate;
}
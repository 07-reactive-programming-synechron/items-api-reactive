package com.classpath.itemsapi.controller;

import com.classpath.itemsapi.model.Order;
import com.classpath.itemsapi.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@RequestMapping ("/api/v1/orders")
@RequiredArgsConstructor
public class OrdersController {
    private final OrderService orderService;

    @GetMapping
    /*
            try{}, catch -> errorContinue
            try,catch, throw ->erroMap
            try,catch return -> errorReturn

     */
    public Flux<Order> fetchAllOrders(){
        return this.orderService.fetchAll();
    }

    @GetMapping("/{orderId}")
    public Mono<Order> fetchOrderById(@PathVariable long orderId){
        return this.orderService.findOrderByOrderId(orderId);
    }

    @PostMapping
    public Mono<Order> saveOrder(@RequestBody @Valid Order order){
        return this.orderService.saveOrder(order);
    }

    @DeleteMapping("/{orderId}")
    public Mono<Order> deleteOrderById(@PathVariable("orderId") long orderId){
        return this.orderService.deleteOrderByOrderId(orderId);
    }
}
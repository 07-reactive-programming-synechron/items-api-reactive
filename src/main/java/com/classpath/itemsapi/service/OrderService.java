package com.classpath.itemsapi.service;

import com.classpath.itemsapi.model.Order;
import com.classpath.itemsapi.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderService {

    private final OrderRepository orderRepository;

    public Flux<Order> fetchAll(){
      log.info("Fetching all the orders from the DB :: ");
      return this.orderRepository.findAll();
    }

    public Mono<Order> findOrderByOrderId(long orderId){
        log.info("Fetching the Order by orderid :: {} ", orderId);
        return this.orderRepository
                        .findById(orderId)
                        .switchIfEmpty(Mono.error(new IllegalArgumentException(" Invalid order id passed")));
    }

    public Mono<Order> saveOrder(Order order){
        return this.orderRepository.save(order);
    }

    public Mono<Order> updateOrderByOrderId(long orderId, Order updatedOrder){

        /*
            Mono<Order> existingOrder = this.orderRepository.findById(orderId);
            Mono<Order> orderMono = existingOrder.map(order -> Order.builder().orderId(orderId).orderDate(updatedOrder.getOrderDate()).email(updatedOrder.getEmail()).build());
            Mono<Order> savedOrder = orderMono.flatMap(order -> this.orderRepository.save(order));
            return savedOrder;
        */
        log.info("Updating the order with orderId :: {} ", orderId);
        return  this.orderRepository.findById(orderId)
                                    .map(order -> Order.builder().orderId(orderId).orderDate(updatedOrder.getOrderDate()).email(updatedOrder.getEmail()).build())
                                    .flatMap(this.orderRepository::save);

    }

    public Mono<Order> deleteOrderByOrderId(long orderId){
        log.info("Deleting the order by its order id :: {}", orderId);
        return this.orderRepository
                        .findById(orderId)
                        .flatMap(order -> this.orderRepository.delete(order).thenReturn(order));
    }
}
package com.classpath.itemsapi.service;

import com.classpath.itemsapi.model.DomainUserDetails;
import com.classpath.itemsapi.model.User;
import com.classpath.itemsapi.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class DomainUserDetailsService implements ReactiveUserDetailsService {

    private final UserRepository userRepository;

    @Override
    public Mono<UserDetails> findByUsername(String username) throws UsernameNotFoundException {
        Mono<User> monoUser = this.userRepository.findByUsername(username);
        return monoUser.map(DomainUserDetails::new);
    }
}
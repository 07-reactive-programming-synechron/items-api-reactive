package com.classpath.itemsapi.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class BcryptPasswordEncoderDemo {

    public static void main(String[] args) {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        String rawPassword = "welcome";

        String cipherText1 = passwordEncoder.encode(rawPassword);
        String cipherText2 = passwordEncoder.encode(rawPassword);
        String cipherText3 = passwordEncoder.encode(rawPassword);

        System.out.println(cipherText1);
        System.out.println(cipherText2);
        System.out.println(cipherText3);

        System.out.println(passwordEncoder.matches("welcome", cipherText1));
        System.out.println(passwordEncoder.matches("welcome", cipherText2));
        System.out.println(passwordEncoder.matches("welcome ", cipherText3));
    }
}
package com.classpath.itemsapi.exception;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.reactive.error.ErrorWebExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Configuration
@RequiredArgsConstructor
@Order(-2)
@Slf4j
public class GlobalExceptionHandler implements ErrorWebExceptionHandler {

    private final ObjectMapper objectMapper;

    @Override
    public Mono<Void> handle(ServerWebExchange serverExchange, Throwable exception) {
        log.info("Came inside the global exception handler function");
        DataBufferFactory dataBufferFactory = serverExchange.getResponse().bufferFactory();
        DataBuffer dataBuffer = null;
        if(exception instanceof IllegalArgumentException){
            serverExchange.getResponse().getHeaders().setContentType(MediaType.APPLICATION_JSON);
            serverExchange.getResponse().setStatusCode(HttpStatus.NOT_FOUND);
            try {
                dataBuffer = dataBufferFactory.wrap(objectMapper.writeValueAsBytes(new Error(100, exception.getMessage())));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        return serverExchange.getResponse().writeWith(Mono.just(dataBuffer));
    }
}

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
class Error {
    private int code;
    private String message;
}
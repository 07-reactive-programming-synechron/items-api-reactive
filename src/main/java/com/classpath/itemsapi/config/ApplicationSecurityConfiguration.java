package com.classpath.itemsapi.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.server.SecurityWebFilterChain;


@Configuration
@RequiredArgsConstructor
@EnableWebFluxSecurity
public class ApplicationSecurityConfiguration {

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    public SecurityWebFilterChain springSecurityWebFilterChain(ServerHttpSecurity httpSecurity){
        httpSecurity.csrf().disable();
        httpSecurity.cors().disable();
        return  httpSecurity
                .authorizeExchange()
                  .pathMatchers(HttpMethod.GET, "/api/v1/orders")
                        .hasAnyRole("USER", "ADMIN")
                  .pathMatchers(HttpMethod.POST, "/api/v1/orders")
                        .hasAnyRole("ADMIN")
                  .anyExchange()
                        .authenticated()
                    .and()
                        .formLogin()
                    .and()
                        .httpBasic()
                    .and()
                .build();
    }
}
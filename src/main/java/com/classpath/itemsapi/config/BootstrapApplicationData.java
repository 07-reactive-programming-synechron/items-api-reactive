package com.classpath.itemsapi.config;

import com.classpath.itemsapi.model.Order;
import com.classpath.itemsapi.repository.OrderRepository;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;


@Configuration
@RequiredArgsConstructor
public class BootstrapApplicationData implements ApplicationListener<ApplicationReadyEvent> {

    private final OrderRepository orderRepository;
    private final Faker faker = new Faker();

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        IntStream.range(1, 100).forEach(index -> {
            Order order = Order.builder().orderDate(faker.date().past(6, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate()).email(faker.internet().emailAddress()).build();
            this.orderRepository.save(order).subscribe(data -> System.out.println("Saved the order "+ data));
        });
    }
}
package com.classpath.itemsapi.repository;

import com.classpath.itemsapi.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.Set;

@Component
@RequiredArgsConstructor
public class UserRepository  {

    private final PasswordEncoder passwordEncoder;

    public Mono<User> findByUsername(String username){

        Map<String, User> userMap = Map.of(
                "kiran", User
                            .builder()
                                .name("kiran")
                                .password(passwordEncoder.encode("welcome"))
                                .email("kiran@gmail.com").roles(Set.of("ROLE_USER"))
                                .salary(45_00_000)
                            .build(),
                "vinay", User
                            .builder()
                                .name("vinay")
                                .password(passwordEncoder.encode("welcome"))
                                .email("kiran@gmail.com").roles(Set.of("ROLE_USER", "ROLE_ADMIN"))
                                .salary(50_00_000)
                            .build()

        );
        return Mono.just(userMap.get(username));
    }
}
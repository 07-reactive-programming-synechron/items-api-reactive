package com.classpath.itemsapi.service;

import com.classpath.itemsapi.model.Order;
import com.classpath.itemsapi.repository.OrderRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Mono;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class OrderServiceTests {

    @Mock
    private OrderRepository orderRepository;

    @InjectMocks
    private OrderService orderService;

    @Test
    public void testSaveOrder(){
        Order order = Order.builder().orderDate(LocalDate.of(2020, 12,12)).email("john@gmail.com").build();
        //set the expectation
        when(orderRepository.save(order)).thenReturn(Mono.just(order));

        Mono<Order> savedOrder = orderService.saveOrder(order);

        Assertions.assertNotNull(savedOrder);
        Order result = savedOrder.block();
        /*savedOrder.subscribe(result -> Assertions.assertNotNull(result));
        savedOrder.subscribe(result -> Assertions.assertEquals(result.getEmail(), "john@gmail.com"));
        */
        //verification
        verify(orderRepository, times(1)).save(order);
    }

    @Test
    public void testFindOrderById(){
        Order order = Order.builder().orderDate(LocalDate.of(2020, 12,12)).email("john@gmail.com").build();
       when(orderRepository.findById(anyLong())).thenReturn(Mono.just(order));

        Mono<Order> savedOrder = orderService.findOrderByOrderId(12L);

        try {
            Order result = savedOrder.block();
        } catch (Exception e) {
            fail("Should not throw any exception");
        }
        //verification
        verify(orderRepository, times(1)).findById(12L);
    }
    @Test
    public void testFindOrderByIdWithInvalidId(){
        Order order = Order.builder().orderDate(LocalDate.of(2020, 12,12)).email("john@gmail.com").build();
       when(orderRepository.findById(anyLong())).thenReturn(Mono.empty());

       try {
            Mono<Order> invalidOrder = orderService.findOrderByOrderId(12L);
            invalidOrder.block();
            fail("Should not come in this block");

       }catch (IllegalArgumentException exception) {
           Assertions.assertTrue(exception instanceof IllegalArgumentException);
       }


        //verification
        verify(orderRepository, times(1)).findById(12L);

    }
}